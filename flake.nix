{
  description = "A Nix Library for Flakes";

  outputs = { self }: let
    _make_lib_internal = { lib, overlays ? [] }: let
      flib = import ./lib/default.nix { lib = lib; };
    in flib.recursiveMerge ([
      lib
      flib
    ] ++ (flib.importLibDir ./lib) ++ (builtins.concatMap (f: f flib) overlays));
    makeCustomLib = lib: dir: _make_lib_internal { lib = lib; overlays = [(f: f.importLibDir dir)]; };
  in {
    makeCustomLib = makeCustomLib;

    overlay = final: prev: {
      lib = _make_lib_internal { lib = prev.lib; };
    };

    overlayCustomLib = dir: final: prev: {
      lib = makeCustomLib prev.lib dir;
    };

    addOverlays = lib: (import ./lib/default.nix { inherit lib;}).importDir;

    templates = {
      trivial = {
        path = ./templates/trivial;
        description = "A basic flake";
      };
      rust = {
        path = ./templates/rust;
        description = "A basic rust flake";
      };
      svelte = {
        path = ./templates/svelte;
        description = "A basic svelte flake";
      };
      python = {
        path = ./templates/python;
        description = "A basic python flake";
      };
      nuxt = {
        path = ./templates/nuxt;
        description = "A basic nuxt flake";
      };
    };

    defaultTemplate = self.templates.trivial;
  };
}
