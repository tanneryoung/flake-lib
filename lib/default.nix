{ lib }:

let
  rakeNixFilesImpl = lib: dir: builtins.filter
    (f: lib.hasSuffix ".nix" f)
    (lib.filesystem.listFilesRecursive dir);
in

{

  recursiveMerge = attrList:
    let f = attrPath:
      lib.zipAttrsWith (n: values:
        if lib.tail values == []
          then lib.head values
        else if lib.all lib.isList values
          then lib.unique (lib.concatLists values)
        else if lib.all lib.isAttrs values
          then f (lib.attrPath ++ [n]) values
        else lib.last values
      );
    in f [] attrList;

  rakeNixFiles = rakeNixFilesImpl lib;

  importDir = dir: (map import (rakeNixFilesImpl lib dir));

  importLibDir = dir: map
    (f: { "${lib.removeSuffix ".nix" (baseNameOf f)}" = import f { inherit lib; }; })
    (builtins.filter
      (f: !(lib.hasSuffix "default.nix" f))
      (rakeNixFilesImpl lib dir)
    );
}
