{ ... }:

{
  cmd = category: name: help: command: {
    inherit category name help command;
  };

  pkg = category: package: {
    inherit category package;
  };
}
