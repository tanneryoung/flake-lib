{
  description = "A Basic Rust Development Template";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    flake-utils.inputs.nixpkgs.follows = "nixpkgs";
    flake-lib.url = "gitlab:tanneryoung/flake-lib";

    naersk.url = "github:nmattia/naersk";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, ... }@inputs:
    with inputs;
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            devshell.overlay
            flake-lib.overlay
            naersk.overlay
            (flake-lib.overlayCustomLib ./lib)
          ] ++ (flake-lib.addOverlays nixpkgs.lib ./overlays);
        };
      in rec {
        packages.my-project = pkgs.naersk.buildPackage {
          pname = "rust";
          root = ./.;
        };
        defaultPackage = packages.my-project;

        apps.my-project = flake-utils.lib.mkApp {
          drv = packages.my-project;
        };
        defaultApp = apps.my-project;

        devShell = pkgs.devshell.mkShell {
          commands = with pkgs; [
            (lib.shell.pkg "rust" cargo)
          ];
          packages = with pkgs; [
            nixfmt
            rustc evcxr
          ];
        };
      });
}
