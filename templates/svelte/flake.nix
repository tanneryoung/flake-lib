{
  description = "A Basic SvelteKit Template";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";

    flake-utils.url = "github:numtide/flake-utils";
    flake-utils.inputs.nixpkgs.follows = "nixpkgs";

    flake-lib.url = "gitlab:tanneryoung/flake-lib";
  };

  outputs = { self, ... }@inputs: with inputs;
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            devshell.overlay
            flake-lib.overlay
            (flake-lib.overlayCustomLib ./lib)
          ] ++ (flake-lib.addOverlays nixpkgs.lib ./overlays);
        };
      in {
        defaultPackage = lib.svelte.build pkgs rec {
          name = "web-${version}";
          version = "0.0.1";
          src = ./.;
        };

        devShell = pkgs.devshell.mkShell {
          commands = with pkgs; [
          ];

          packages = with pkgs; [
            nixfmt
            nodejs
          ];

          env = [
            { name = "HELLO"; value = "WORLD"; }
          ];
        };
      });
}

