final: prev: {
  esbuild = final.pkgs.buildGoModule rec {
    pname = "esbuild";
    version = "0.13.13";
    src = final.pkgs.fetchFromGitHub {
      owner = "evanw";
      repo = pname;
      rev = "v${version}";
      sha256 = "0b4hmlglhpqd8rp73n8jkpbjfq2ddl1r9ac9ayhdbl5xij98ak45";
    };
    vendorSha256 = "sha256-QPkBR+FscUc3jOvH7olcGUhM6OW4vxawmNJuRQxPuGs=";
  };
}
