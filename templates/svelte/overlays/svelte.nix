final: prev: {
  mkSveltePackage = kwargs: final.pkgs.mkYarnPackage (kwargs // {
    configurePhase = ''
      rm -rf build node_modules || true
      cp -r $node_modules node_modules
      chmod -R +w node_modules

      # TODO: This might only work on the x86_64-linux system.
      ln -sf ${final.pkgs.esbuild}/bin/esbuild node_modules/esbuild/bin/esbuild
      ln -sf ${final.pkgs.esbuild}/bin/esbuild node_modules/esbuild-linux-64/bin/esbuild
    '';
    installPhase = ''
      yarn --offline --frozen-lockfile build
      mkdir $out
      mv build/* $out
    '';
    distPhase = "true";
  });
}
