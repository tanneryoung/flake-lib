import adapter from '@sveltejs/adapter-static';
import {windi} from 'svelte-windicss-preprocess';

/** @type {import('@sveltejs/kit').Config} */
export default {
  preprocess: [
    windi({})
  ],
  kit: {
    adapter: adapter({}),
    ssr: false,
    target: '#svelte'
  }
};
