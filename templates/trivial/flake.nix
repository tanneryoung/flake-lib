{
  description = "A Basic Development Template";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";

    flake-utils.url = "github:numtide/flake-utils";
    flake-utils.inputs.nixpkgs.follows = "nixpkgs";

    flake-lib.url = "gitlab:tanneryoung/flake-lib";
  };

  outputs = { self, ... }@inputs: with inputs;
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            devshell.overlay
            flake-lib.overlay
            (flake-lib.overlayCustomLib ./lib)
          ] ++ (flake-lib.addOverlays nixpkgs.lib ./overlays);
        };
      in {
        devShell = pkgs.devshell.mkShell {
          commands = with pkgs; [
            (lib.shell.pkg "misc" hello)
            (lib.shell.cmd "misc" "goodbye" "Say Goodbye!" "echo 'Goodbye!'")
          ];

          packages = with pkgs; [
            nixfmt
          ];

          env = [
            { name = "HELLO"; value = "WORLD"; }
          ];
        };
      });
}
